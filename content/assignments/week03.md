---
title: "Computer-Aided Design"
week: "03"
---

Here we explore different CAD packages as well as vector and raster graphics software.