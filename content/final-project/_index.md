---
title: "Final Project"
---
______
&nbsp;
## This page describe the final project progress.
______

&nbsp;&nbsp;

## A Flipped Plate: shape variations

&nbsp;&nbsp;

>> *[Sketch for the display of the installtion]*  
![ProjectImg](../final-project-idea.png)  
 


The project title is "A Flipped Plate: shape variations".  
It will show the kinetic installation as the final outcome.  
This is a part of Yoona's artistic practice, exploring the idea of coexisting ambivalence.

>> *The flipped wooden plate is two-sided. At first glance, the plate appears to have front and back sides. However, as the audience moves around the installation, each side of the plate simply switches to another side, creating a natural and ambient sound by touching the other plate. The audience eventually walks around the wooden object, just observing the plates flipping while listening to their sound and watching their changes, not knowing which side is actually front or back. ... <the artist note>*

This sculptural installation uses the movement of flipping the wood blocks by strings as a system of the toy, [Jacob's ladder ⇱](https://en.wikipedia.org/wiki/Jacob's_ladder_(toy)). As the previous practice for the artistic concept, 'coexisting ambivalence', I made one module (dimension: W 120 x H 1660 x D 260 mm) with the linked twenty-five sets of wooden plates (length: 1540mm) and the aluminum structure with the electronic components. Especially, the first practice includes the Infrared Array sensor to detect the human body's temperature for generating the movement of the installation.

In the new practice during the Digital Fabrication Course, meanwhile, the installation's structure will have various forms with different materials for the technical and mechanical parts. On top of that, the new practice will not include the interactive factor.

&nbsp;&nbsp;

>> *[Video of the previous practice]*  


&nbsp;&nbsp;
>> *[Installation view of the previous practice]*  

>> ![ProjectImg](../installation-view.JPG)  
>> ![ProjectImg](../technical-structure.png)  

&nbsp;&nbsp;

-----
#### Reference works

- [Jacob's Wall (2018) by Parker Heyl ⇱](https://www.parkerheyl.com/?pgid=j9zl8qi9-0b3bc0a6-5487-463b-9b37-b29a25c6d8f3)

- [Katakata (2015) by Kirsty Keatch ⇱](https://www.kirstykeatch.com/katakata.html)
 


&nbsp;&nbsp;

-----
#### Project guideline

- What does it do?  
- Who's done what beforehand?  
- What did you design?  
- What materials and components were used?  
- Where did they come from?  
- How much did they cost?  
- What parts and systems were made?  
- What processes were used?  
- What questions were answered?  
- What worked?  
- What didn't?  
- How was it evaluated?  
- What are the implications?  
- At the end of the course you should have a summary slide and one-minute video explaining the conception, construction and operation of your final project.

- Your project should incorporate:  
2D and 3D design,  additive and subtractive fabrication processes, electronics design and production, embedded microcontroller interfacing and programming as well as system design and packaging.  